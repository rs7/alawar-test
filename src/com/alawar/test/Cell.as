package com.alawar.test {
    
    public class Cell extends Cell_design {
        
        public static const SIZE:Number = 50;
        
        public function Cell() {
            gotoAndStop("none");
        }
        
        private var _color:String;
        
        public function get color():String {
            return _color;
        }
        
        public function set color(value:String):void {
            _color = value;
            gotoAndStop(value);
        }
        
    }
    
}