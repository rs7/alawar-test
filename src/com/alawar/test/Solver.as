package com.alawar.test {
    
    public class Solver {
        
        DIRECTION_TABLE[Direction.DOWN] = new CellIndex(0, -1);
        DIRECTION_TABLE[Direction.LEFT] = new CellIndex(1, 0);
        DIRECTION_TABLE[Direction.RIGHT] = new CellIndex(-1, 0);
        DIRECTION_TABLE[Direction.UP] = new CellIndex(0, 1);
        
        public static var DIRECTION_TABLE:Object = {};
        private static const SEARCH_LENGTH:int = 3;
        
        public function Solver(field:Field):void {
            this.field = field;
        }
        
        private var field:Field;
        private var data:Data;
        
        public function getSolve():Vector.<Hint> {
            var result:Vector.<Hint> = new Vector.<Hint>;
            
            var size:int = field.size;
            
            data = new Data(size);
            
            var x:int;
            var y:int;
            
            data.startNewDirection(Direction.RIGHT);
            for (y = 0; y < size; y++) {
                data.startNewLine();
                for (x = 0; x < size; x++) {
                    data.step(x, y, field.getCell(new CellIndex(x, y)).color);
                }
            }
            
            data.startNewDirection(Direction.LEFT);
            for (y = 0; y < size; y++) {
                data.startNewLine();
                for (x = size - 1; x >= 0; x--) {
                    data.step(x, y, field.getCell(new CellIndex(x, y)).color);
                }
            }
            
            data.startNewDirection(Direction.DOWN);
            for (x = 0; x < size; x++) {
                data.startNewLine();
                for (y = 0; y < size; y++) {
                    data.step(x, y, field.getCell(new CellIndex(x, y)).color);
                }
            }
            
            data.startNewDirection(Direction.UP);
            for (x = 0; x < size; x++) {
                data.startNewLine();
                for (y = size - 1; y >= 0; y--) {
                    data.step(x, y, field.getCell(new CellIndex(x, y)).color);
                }
            }
            
            var cellDataList:Vector.<Object> = data.getResult();
            
            var cellDataIndex:int = cellDataList.length;
            while (cellDataIndex--) {
                var cellData:Object = cellDataList[cellDataIndex];
                for (var color:String in cellData.colors) {
                    if (color == cellData.currentColor) continue;
                    var colorData:Object = cellData.colors[color];
                    if (colorData.summ < SEARCH_LENGTH || colorData.sides < 2) continue;
                    for (var direction:String in colorData.directions) {
                        var directionSumm:int = colorData.directions[direction];
                        
                        if (directionSumm < 1) continue;
                        if (colorData.summ - directionSumm + 1 < SEARCH_LENGTH) continue;
                        
                        if (
                            colorData.lines[directionType(direction)] - directionSumm + 1 < SEARCH_LENGTH && 
                            colorData.lines[anti(directionType(direction))] + 1 < SEARCH_LENGTH
                        ) continue;
                        
                        trace(cellDataIndex, direction);
                        
                        result.push(new Hint(
                            
                            CellIndex.summ(
                                new CellIndex(
                                    cellDataIndex % size,
                                    int(cellDataIndex / size)
                                ), DIRECTION_TABLE[direction]
                            ), 
                            direction
                        ));
                    }
                }
            }
            
            return result;
        }
    
    }
    
}

import com.alawar.test.CellColor;
import com.alawar.test.Direction;
import com.alawar.test.Solver;

internal class Data {
    
    public function Data(size:int) {
        this.size = size;
        
        data = new Vector.<Object>(Math.pow(size, 2));
        
        var cellIndex:int = data.length;
        while (cellIndex--) {
            
            var cellData:Object = { 
                currentColor:null,
                colors: { }
            }
            
            for each(var color:String in CellColor.COLORS) {
                var colorData:Object = {
                    sides:0,
                    summ:0,
                    directions: { },
                    lines: {
                        h:0,
                        v:0
                    }
                }
                
                for (var direction:String in Solver.DIRECTION_TABLE) {
                    colorData.directions[direction] = 0;
                }
                
                cellData.colors[color] = colorData;
            }
            
            data[cellIndex] = cellData;
            
        }
    }
    
    private var data:Vector.<Object>;
    private var direction:String;
    private var prevColor:String;
    private var size:int;
    private var summ:int;
    
    public function getResult():Vector.<Object> {
        return data;
    }
    
    public function startNewDirection(direction:String):void {
        this.direction = direction;
    }
    
    public function startNewLine():void {
        prevColor = null;
    }
    
    public function step(x:int, y:int, color:String):void {
        var pos:int = x + size * y;
        
        data[pos].currentColor = color;
        
        if (prevColor != null) {
            data[pos].colors[prevColor].summ += summ;
            data[pos].colors[prevColor].sides++;
            data[pos].colors[prevColor].directions[direction] = summ;
            data[pos].colors[prevColor].lines[directionType(direction)] += summ;
        }
        
        if (prevColor == color) {
            summ++;
        } else {
            summ = 1;
        }
        
        prevColor = color;
    }
}

internal function directionType(direction:String):String {
    switch (direction) {
        case Direction.DOWN:
        case Direction.UP:
            return "v";
            
        case Direction.LEFT:
        case Direction.RIGHT:
            return "h";
        default:
            return "";
    }
}

internal function anti(directionType:String):String {
    switch (directionType) {
        case "v": return "h";
        case "h": return "v";
        default: return "";
    }
}
