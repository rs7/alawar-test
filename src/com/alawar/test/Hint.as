package com.alawar.test {
    
    public class Hint {
        
        public function Hint(index:CellIndex, direction:String):void {
            this.index = index;
            this.direction = direction;
            
        }
        
        public var direction:String;
        public var index:CellIndex;
        
    }
    
}