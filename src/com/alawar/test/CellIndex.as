package com.alawar.test {
    
    public class CellIndex {
        
        public static function summ(indexA:CellIndex, indexB:CellIndex):CellIndex {
            return new CellIndex(
                indexA.x + indexB.x,
                indexA.y + indexB.y
            );
        }
        
        public function CellIndex(x:int, y:int):void {
            this.x = x;
            this.y = y;
        }
        
        public var x:int;
        public var y:int;
    }
    
}