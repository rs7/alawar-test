package com.alawar.test {
    
    public class CellColor {
        
        public static const BLUE:String = "blue";
        public static const GREEN:String = "green";
        public static const RED:String = "red";
        public static const YELLOW:String = "yellow";
        
        public static const COLORS:Vector.<String> = new <String>[
            BLUE,
            GREEN,
            RED,
            YELLOW
        ];
        
        public static function getRandomColor():String {
            return COLORS[int(Math.random() * COLORS.length)];
        }        
        
    }
    
}