package com.alawar.test {
    
    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    
    public class Field extends Sprite {
        
        public function Field(size:int):void {
            this.size = size;
            init();
            generate();
        }
        
        private var cells:Vector.<Cell>;
        private var hinters:Vector.<CellHinter>;
        public var size:int;
        
        private function allowableIndex(index:CellIndex):Boolean {
            return index.x >= 0 && index.x < size && index.y >= 0 && index.y < size;
        }
        
        public function generate():void {
            for each(var cell:Cell in cells) {
                cell.color = CellColor.getRandomColor();
            }
        }
        
        public function getCell(index:CellIndex):Cell {
            if (!allowableIndex(index)) return null;
            return cells[indexToPos(index)];
        }
        
        public function hintersDisableAll():void {
            var i:int = hinters.length;
            while (i--) {
                hinters[i].disableAll();
            }
        }
        
        public function hintersEnableOne(hint:Hint):void {
            hinters[indexToPos(hint.index)].enable(hint.direction);
        }
        
        private function indexToPoint(index:CellIndex):Point {
            return new Point(
                Cell.SIZE * index.x,
                Cell.SIZE * index.y
            );
        }
        
        private function indexToPos(index:CellIndex):int {
            return index.y * size + index.x;
        }
        
        private function init():void {
            cells = new Vector.<Cell>(Math.pow(size, 2));
            
            var i:int;
            var point:Point;
            
            i = cells.length;
            while (i--) {
                cells[i] = new Cell;
                point = indexToPoint(posToIndex(i));
                cells[i].x = point.x;
                cells[i].y = point.y;
                addChild(cells[i]);
            }
            
            hinters = new Vector.<CellHinter>(cells.length);
            i = hinters.length;
            while (i--) {
                hinters[i] = new CellHinter;
                point = indexToPoint(posToIndex(i));
                hinters[i].x = point.x;
                hinters[i].y = point.y;
                addChild(hinters[i]);
            }
        }
        
        private function posToIndex(pos:int):CellIndex {
            return new CellIndex(
                pos % size,
                int(pos / size)
            );
        }
        
    }
    
}