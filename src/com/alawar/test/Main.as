package com.alawar.test {
    
    import flash.display.Sprite;
    import flash.events.MouseEvent;
    
    public class Main extends Sprite {
        
        private static const FIELD_SIZE:int = 7;
        
        public function Main():void {
            init();
        }
        
        private var field:Field;
        private var generateButton:GenerateButton;
        private var solver:Solver;
        
        private function init():void {
            
            field = new Field(FIELD_SIZE);
            field.x = 50;
            field.y = 50;
            addChild(field);
            
            solver = new Solver(field);
            showHints();
            
            generateButton = new GenerateButton;
            generateButton.addEventListener(MouseEvent.CLICK, generateButton_clickHandler);
            generateButton.x = 750;
            generateButton.y = 550;
            addChild(generateButton);
        }
        
        private function showHints():void {
            field.hintersDisableAll();
            for each(var hint:Hint in solver.getSolve()) {
                field.hintersEnableOne(hint);
            }
        }
        
        private function generateButton_clickHandler(event:MouseEvent):void {
            field.generate();
            showHints();
        }
        
    }
    
}