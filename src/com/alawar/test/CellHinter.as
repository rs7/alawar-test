package com.alawar.test {
    
    import flash.display.MovieClip;
    
    public class CellHinter extends CellHinter_design {
        
        public function CellHinter():void {
            init();
        }
        
        private var indicators:Object;
        
        public function disableAll():void {
            for each(var indicator:MovieClip in indicators) {
                indicator.visible = false;
            }
        }
        
        public function enable(direction:String):void {
            (indicators[direction] as MovieClip).visible = true;
        }
        
        private function init():void {
            indicators = {}
            indicators[Direction.DOWN] = down;
            indicators[Direction.LEFT] = left;
            indicators[Direction.RIGHT] = right;
            indicators[Direction.UP] = up;
            
            disableAll();
        }
        
    }
    
}